Prerequisites

1. Install Eclipse or any Editor, JAVA version "15.0.2", Maven apache-maven-3.8.1 and set up the Envirnoment Variables.

2. Chromedriver 96 is needed and chromedriver.exe is already provided inside the project in drivers folder.

3. Chrome Browser should be Installed in machine of version 96.

Usage

1. After the above setup Open the project in Editor.

2. Run Maven Compile.

3. Triggering Tests:
   3.1 Once the dependencies got downloaded create Run configuration by mentioning Projectname in Base Directory and 
   give clean install command in Goals and click Run.
   OR
   3.2 Directly click Maven clean OR Maven Install.
   OR
   3.3 Tests can also be triggered from command line using command mvn clean install and before running the command open the command prompt
   from project location.
   OR 
   3.4 Run directly CucumberRunner.java class via Junit.
   
4. Once the Tests Execution finished refresh the project and open the report.html from target folder to see the results, also attached one
   sample report for reference which have the results of both UI and API Test cases.
 
   


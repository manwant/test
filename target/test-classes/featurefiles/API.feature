Feature: Calling Current Weather and Hourly ForeCast APIs

@Smoke
  Scenario: Validate Data and Statecode in GET Current Weather Data API
	Given   Hit the Current Weather API with URL "/current" and lat "40.730610" long "-73.935242"

@Smoke
  Scenario: Validate Timestamp and Weather in GET Hourly Forecast API
	Given   Hit the Hourly Forecast API with URL "/forecast/hourly" and postalCode "28546"
	                   
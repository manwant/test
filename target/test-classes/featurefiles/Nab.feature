Feature: Booking an Appointment for New Home loan

@Smoke
  Scenario: Filling the New Home Loan Callback Form
	Given   Open the NAB application.  
	And     Select Personal tab and then choose Home Loan options.
	Then    Select Book An Appointment Option and click Buying an Property Option.
	Then    Select an Applicant and click Next Button.
	And     Choose Income with Deposit and click Next Button.
	Then    Enter Suburb "Melbourne VIC,Australia" and Click Next Button.
	And     Select Over the Phone Option with Date and Time.
	Then    Fill the following Contact Details and check for Confirmation Message. 
          | FirstName  | LastName | PreferredFirstName | Email              | MobileNumber|
          | Manwantjot | Gill     | Manwant            | appium87@gmail.com | +91411312421|   

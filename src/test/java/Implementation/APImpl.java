package Implementation;

import org.apache.logging.log4j.LogManager;

import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import helpers.ConfigReader;
import io.restassured.RestAssured;
import io.restassured.config.HttpClientConfig;
import io.restassured.config.RestAssuredConfig;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class APImpl {

	private static Logger logger = LogManager.getLogger(APImpl.class);

	public static boolean GETCurrentWeatherDataAPI(String endpointname, String lat, String lon) {
		logger.info("Enter GETCurrentWeatherDataAPI function");
		logger.info("Params: " + endpointname + " " + lat + " " + lon);
		boolean isStateCodeFound = false;
		try {
			settingTheEndpointURL(endpointname);
			aa: for (int counter = 1; counter <= ConfigReader.getIntValue("APIRetryCount"); counter++) {
				logger.info("API Calling Attempt : " + counter);
				Response response = getCurrentWeatherAPIStatusCode(endpointname, lat, lon);
				logger.info("Response Status Code coming: " + response.getStatusCode());
				if (response.getStatusCode() == 200) {
					logger.info("Got the Expected Response from API");
					String finalResponse = response.getBody().asString();
					JSONObject obj = new JSONObject(finalResponse);
					JSONArray dataArray = obj.getJSONArray("data");
					int dataArrayLength = dataArray.length();
					logger.info("Length of data array: " + dataArrayLength);
					bb: for (int i = 0; i < dataArrayLength; i++) {
						String stateCode = dataArray.getJSONObject(i).getString("state_code");
						logger.info("stateCode coming: " + stateCode);
						if (stateCode != null) {
							logger.info("Got the Expected state Code");
							isStateCodeFound = true;
							break bb;
						}
					}
					break aa;
				} else {
					logger.info("Not got the Expected Status Code Checking again.");
					if (counter == ConfigReader.getIntValue("APIRetryCount")) {
						logger.info("All Retry Attempts Finished");
					}
				}
			}
		} catch (Exception e) {
			logger.info("Landed in catch block", e);

		}
		logger.info("Exit GETCurrentWeatherDataAPI function");
		return isStateCodeFound;
	}

	public static boolean GETHourlyForecastDataAPI(String endpointname, String postalCode) {
		logger.info("Enter GETHourlyForecastDataAPI function");
		logger.info("Params: " + endpointname + " " + postalCode);
		boolean isWeatherDataFound = false;
		try {
			settingTheEndpointURL(endpointname);
			aa: for (int counter = 1; counter <= ConfigReader.getIntValue("APIRetryCount"); counter++) {
				logger.info("API Calling Attempt : " + counter);
				Response response = getHourlyForecastAPIStatusCode(endpointname, postalCode);
				logger.info("Response Status Code coming: " + response.getStatusCode());
				if (response.getStatusCode() == 200) {
					logger.info("Got the Expected Response from API");
					String finalResponse = response.getBody().asString();
					logger.info("finalResponse: " + finalResponse);
					JSONObject obj = new JSONObject(finalResponse);
					JSONArray dataArray = obj.getJSONArray("data");
					int dataArrayLength = dataArray.length();
					logger.info("Length of data array: " + dataArrayLength);
					bb: for (int i = 0; i < dataArrayLength; i++) {
						String timeStampUTC = dataArray.getJSONObject(i).getString("timestamp_utc");
						logger.info("timeStampUTC coming: " + timeStampUTC);
						if (timeStampUTC != null) {
							logger.info("Got the Expected Time");
							JSONObject weather = dataArray.getJSONObject(i).getJSONObject("weather");
							logger.info("Weather coming: " + weather);
							if (weather != null) {
								if (i == dataArrayLength - 1) {
									isWeatherDataFound = true;
									break bb;
								}
							}
						}
					}
					break aa;
				} else {
					logger.info("Not got the Expected Status Code Checking again.");
					if (counter == ConfigReader.getIntValue("APIRetryCount")) {
						logger.info("All Retry Attempts Finished");
					}
				}
			}
		} catch (Exception e) {
			logger.info("Landed in catch block", e);

		}
		logger.info("Exit GETCurrentWeatherDataAPI function");
		return isWeatherDataFound;
	}

	public static void settingTheEndpointURL(String endpointname) {
		logger.info("Endpoint Name coming: " + endpointname);
		RestAssured.useRelaxedHTTPSValidation();
		RestAssured.config = RestAssuredConfig.config()
				.httpClient(HttpClientConfig.httpClientConfig().setParam("http.connection.timeout", 5000)
						.setParam("http.socket.timeout", 5000).setParam("http.connection-manager.timeout", 5000));
		RestAssured.baseURI = ConfigReader.getValue("APIUrl") + endpointname;
		logger.info("Final Endpoint URL: " + RestAssured.baseURI);

	}

	public static RequestSpecification settingAPIHeaders() {
		logger.info("Enter settingAPIHeaders function");
		RequestSpecification request = RestAssured.given().header(ConfigReader.getValue("Headerkey"),
				ConfigReader.getValue("Headervalue"));
		logger.info("Exit settingAPIHeaders function");
		return request;

	}

	public static Response getCurrentWeatherAPIStatusCode(String endpointname, String lat, String lon) {
		Response response = settingAPIHeaders().queryParam("lat", lat).queryParam("lon", lon)
				.queryParam("key", ConfigReader.getValue("APIkey")).when().get(endpointname);
		logger.info("CurrentWeatherAPI Response Status Code coming: " + response);
		return response;

	}

	public static Response getHourlyForecastAPIStatusCode(String endpointname, String postalCode) {
		Response response = settingAPIHeaders().queryParam("postal_code", postalCode)
				.queryParam("key", ConfigReader.getValue("APIkey")).when().get(endpointname);
		logger.info("HourlyForecastAPI Response Status Code coming: " + response);
		return response;

	}

}

package Implementation;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.logging.log4j.LogManager;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.remote.RemoteWebElement;
import helpers.ConfigReader;
import helpers.DriverHelper;
import helpers.MiscHelper;
import pageobjects.NabPO;
import stepdefinitions.Hooks;

public class NabImp {

	private static Logger logger = LogManager.getLogger(NabImp.class);
	private static int dateincrement;

	public static boolean openingNabURL() {
		boolean isExpectedURLOpened = false;
		try {
			logger.info("Enter openingNabURL function");
			Hooks.testDriver.getWebDriver().get(ConfigReader.getValue("URL"));
			MiscHelper.domLoading(Hooks.testDriver.getWebDriver());
			logger.info("Opened URL: "+Hooks.testDriver.getWebDriver().getCurrentUrl());
			if (Hooks.testDriver.getWebDriver().getCurrentUrl().contains(ConfigReader.getValue("URL"))) {
				isExpectedURLOpened = true;
			}

		} catch (Exception e) {
			logger.error("Landed in Catch block", e);
		}
		logger.info("Exit openingNabURL function");
		return isExpectedURLOpened;

	}

	public static boolean selectingPersonalTabandClickHomeLoans() {
		boolean isSubMenuOpened = false;
		try {
			logger.info("Enter selectingPersonalTabandClickHomeLoans function");
			DriverHelper.click(Hooks.testDriver.getWebDriver(), NabPO.Personaloption);
			MiscHelper.checkElementVisibilty(Hooks.testDriver.getWebDriver(),
					"getElementsByClassName('dropdown mega-menu active').length");
			DriverHelper.click(Hooks.testDriver.getWebDriver(), NabPO.Homesloans);
			DriverHelper.click(Hooks.testDriver.getWebDriver(), NabPO.HomesLoansSubmenu);
			isSubMenuOpened = MiscHelper.checkElementVisibilty(Hooks.testDriver.getWebDriver(),
					"getElementsByClassName('cmp-list__item-title').length");

		} catch (Exception e) {
			logger.error("Landed in Catch block", e);
		}
		logger.info("Exit selectingPersonalTabandClickHomeLoans function");
		return isSubMenuOpened;

	}

	public static boolean clickingOnBookAppointmentOptionandSelectBuyProperty() {
		boolean isElementClicked = false;
		try {
			logger.info("Enter clickingOnBookAppointmentOptionandSelectBuyProperty function");
			DriverHelper.click(Hooks.testDriver.getWebDriver(), NabPO.BookAnappointment);
			RemoteWebElement shadowElement = MiscHelper.handlingShadowElementInDOM(NabPO.ParentShadowelement,
					Hooks.testDriver.getWebDriver());
			DriverHelper.shadowElementClick(Hooks.testDriver.getWebDriver(),
					shadowElement.findElement(By.cssSelector(NabPO.BuyingAProperty)));
			isElementClicked = true;
		} catch (Exception e) {
			logger.error("Landed in Catch block", e);
		}
		logger.info("Exit clickingOnBookAppointmentOptionandSelectBuyProperty function");
		return isElementClicked;

	}

	public static boolean selectanApplicantAndClickNextButton() {
		logger.info("Enter selectanApplicantAndClickNextButton function");
		boolean isElementClicked = false;
		try {

			RemoteWebElement shadowElement = MiscHelper.handlingShadowElementInDOM(NabPO.ParentShadowelement,
					Hooks.testDriver.getWebDriver());
			DriverHelper.shadowElementClick(Hooks.testDriver.getWebDriver(),
					shadowElement.findElement(By.cssSelector(NabPO.OneApplicant)));
			DriverHelper.scrollbyview(Hooks.testDriver.getWebDriver(),
					shadowElement.findElement(By.cssSelector(NabPO.NextButton)));
			DriverHelper.shadowElementClick(Hooks.testDriver.getWebDriver(),
					shadowElement.findElement(By.cssSelector(NabPO.NextButton)));
			isElementClicked = true;
		} catch (Exception e) {
			logger.error("Landed in Catch block", e);
		}
		logger.info("Exit selectanApplicantAndClickNextButton function");
		return isElementClicked;
	}

	public static boolean selectIncomeAndClickNextButton() {
		logger.info("Enter selectIncomeAndClickNextButton function");
		boolean isElementClicked = false;
		try {
			RemoteWebElement shadowElement = MiscHelper.handlingShadowElementInDOM(NabPO.ParentShadowelement,
					Hooks.testDriver.getWebDriver());
			DriverHelper.shadowElementClick(Hooks.testDriver.getWebDriver(),
					shadowElement.findElement(By.cssSelector(NabPO.FullORPartTimeIncome)));
			DriverHelper.scrollbyview(Hooks.testDriver.getWebDriver(),
					shadowElement.findElement(By.cssSelector(NabPO.NextButton)));
			DriverHelper.shadowElementClick(Hooks.testDriver.getWebDriver(),
					shadowElement.findElement(By.cssSelector(NabPO.NextButton)));
			isElementClicked = true;
		} catch (Exception e) {
			logger.error("Landed in Catch block", e);
		}

		logger.info("Exit selectIncomeAndClickNextButton function");
		return isElementClicked;
	}

	public static boolean selectDepositAndClickNextButton() {
		logger.info("Enter selectDepositAndClickNextButton function");
		boolean isElementClicked = false;
		try {
			RemoteWebElement shadowElement = MiscHelper.handlingShadowElementInDOM(NabPO.ParentShadowelement,
					Hooks.testDriver.getWebDriver());
			DriverHelper.shadowElementClick(Hooks.testDriver.getWebDriver(),
					shadowElement.findElement(By.cssSelector(NabPO.MoneySavedDeposit)));
			DriverHelper.scrollbyview(Hooks.testDriver.getWebDriver(),
					shadowElement.findElement(By.cssSelector(NabPO.NextButton)));
			DriverHelper.shadowElementClick(Hooks.testDriver.getWebDriver(),
					shadowElement.findElement(By.cssSelector(NabPO.NextButton)));
			isElementClicked = true;
		} catch (Exception e) {
			logger.error("Landed in Catch block", e);
		}

		logger.info("Exit selectDepositAndClickNextButton function");
		return isElementClicked;
	}

	public static boolean enterSuburbAndClickNextButton(String subUrbName) {
		logger.info("Enter enterSuburbAndClickNextButton function with param: " + subUrbName);
		boolean isElementClicked = false;
		try {
			RemoteWebElement shadowElement = MiscHelper.handlingShadowElementInDOM(NabPO.ParentShadowelement,
					Hooks.testDriver.getWebDriver());
			DriverHelper.shadowElementTypeText(Hooks.testDriver.getWebDriver(),
					shadowElement.findElement(By.cssSelector(NabPO.SubUrb)), subUrbName);
			DriverHelper.sleep(3);
			DriverHelper.shadowElementClick(Hooks.testDriver.getWebDriver(),
					shadowElement.findElement(By.cssSelector(NabPO.SelectSubUrb)));
			DriverHelper.scrollbyview(Hooks.testDriver.getWebDriver(),
					shadowElement.findElement(By.cssSelector(NabPO.SubUrbNextButton)));
			DriverHelper.shadowElementClick(Hooks.testDriver.getWebDriver(),
					shadowElement.findElement(By.cssSelector(NabPO.SubUrbNextButton)));
			isElementClicked = true;
		} catch (Exception e) {
			logger.error("Landed in Catch block", e);
		}

		logger.info("Exit enterSuburbAndClickNextButton function");
		return isElementClicked;
	}

	public static boolean selectOverthePhoneOptionwithDateTime() {
		logger.info("Enter selectOverthePhoneOptionwithDateTime function");
		boolean isElementClicked = false;
		try {
			RemoteWebElement parentShadowElement = MiscHelper.handlingShadowElementInDOM(NabPO.ParentShadowelement,
					Hooks.testDriver.getWebDriver());
			DriverHelper.shadowElementClick(Hooks.testDriver.getWebDriver(),
					parentShadowElement.findElement(By.cssSelector(NabPO.OverthePhone)));
			RemoteWebElement shadowElement = null;
			LocalDateTime currentDate = LocalDateTime.now();
			String currentDateAndYear = DateTimeFormatter.ofPattern("yyyy/MM/dd").format(currentDate);
			logger.info("currentDateAndYear: " + currentDateAndYear);
			int date = Integer
					.parseInt(DateTimeFormatter.ofPattern("yyyy/MM/dd").format(currentDate).split("/")[2].trim());
			logger.info("Current Date Coming: " + date);
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd", Locale.ENGLISH);
			LocalDate date2 = LocalDate.parse(currentDateAndYear, dtf);
			DayOfWeek dow = date2.getDayOfWeek();
			logger.info("Day Name coming: " + dow);
			int clickCount = (int) (ConfigReader.getIntValue("Days") - MiscHelper.retrundayasperAPPCalender(dow.toString()));
			logger.info("clickCount: " + clickCount);
			dateincrement = date;
			logger.info("Value in dateincrement before start: " + dateincrement);
			String isNoAvailableTime = "NA";
			shadowElement = MiscHelper.handlingShadowElementInDOM(NabPO.ParentShadowelement,
					Hooks.testDriver.getWebDriver());
			for (int i = 0; i < clickCount; i++) {
				try {
					isNoAvailableTime = shadowElement.findElement(By.cssSelector(NabPO.NoAvailableTime)).getText();
				} catch (Exception e) {
					logger.error("Landed in catch block: " + e);
				}
				logger.info("isNoAvailableTimesFound Status: " + isNoAvailableTime);
				if (isNoAvailableTime.contains("No")) {
					logger.info("Need to select Other date");
					dateincrement = dateincrement + 1;
					logger.info("Date to Click: " + dateincrement);
					DriverHelper.shadowElementClick(Hooks.testDriver.getWebDriver(), shadowElement.findElement(
							By.cssSelector(NabPO.DateToSelect.replace("xxx", String.valueOf(dateincrement)))));
					System.out.println("Waiting....");
					DriverHelper.sleep(8);
				} else {
					logger.info("No Need to select Other date");
					break;

				}
			}
			try {
				logger.info("First Attempt");
				DriverHelper.shadowElementClick(Hooks.testDriver.getWebDriver(),
						shadowElement.findElement(By.cssSelector(NabPO.MorningTimeToSelect)));
				logger.info("First Attempt Done");
			} catch (Exception exception) {
				try {
					logger.info("Second Attempt");
					DriverHelper.shadowElementClick(Hooks.testDriver.getWebDriver(),
							shadowElement.findElement(By.cssSelector(NabPO.NoonTimeToSelect)));
					logger.info("Second Attempt Done");
				} catch (Exception e) {
					logger.info("Third Attempt");
					DriverHelper.shadowElementClick(Hooks.testDriver.getWebDriver(),
							shadowElement.findElement(By.cssSelector(NabPO.EveningTimeToSelect)));
					logger.info("Third Attempt Done");
				}
			}
			logger.info("Date has been Selected");

			try {
				logger.info("Scrolling Next Button in Try Block");
				DriverHelper.scrollbyview(Hooks.testDriver.getWebDriver(),
						shadowElement.findElement(By.cssSelector(NabPO.DateTimeNextButton2)));
				logger.info("Scrolled Next Button in Try Block");
				logger.info("Clicking the Next Button");
				DriverHelper.shadowElementClick(Hooks.testDriver.getWebDriver(),
						shadowElement.findElement(By.cssSelector(NabPO.DateTimeNextButton2)));
				isElementClicked = true;
			} catch (Exception e) {
				logger.info("Scrolling Next Button in Catch Block");
				String scrollElementIntoMiddle = "var viewPortHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);"
						+ "var elementTop = arguments[0].getBoundingClientRect().top;"
						+ "window.scrollBy(0, elementTop-(viewPortHeight/2));";
				((JavascriptExecutor) Hooks.testDriver.getWebDriver()).executeScript(scrollElementIntoMiddle,
						shadowElement.findElement(By.cssSelector(NabPO.DateTimeNextButton2)));
				logger.info("Scrolled Next Button in Catch Block");
				logger.info("Clicking the Next Button");
				DriverHelper.shadowElementClick(Hooks.testDriver.getWebDriver(),
						shadowElement.findElement(By.cssSelector(NabPO.DateTimeNextButton2)));
				isElementClicked = true;
			}
		} catch (Exception e) {
			logger.error("Landed in Catch block", e);
		}

		logger.info("Exit selectOverthePhoneOptionwithDateTime function");
		return isElementClicked;
	}

	public static boolean fillTheContactDetails(List<Map<String, String>> rows) {
		logger.info("Enter fillTheContactDetails function with param: " + rows);
		boolean isAppointmentbooked = false;
		try {
			RemoteWebElement shadowElement = MiscHelper.handlingShadowElementInDOM(NabPO.ParentShadowelement,
					Hooks.testDriver.getWebDriver());
			for (Map<String, String> columns : rows) {
				logger.info("columns: " + columns + " " + rows);
				DriverHelper.shadowElementTypeText(Hooks.testDriver.getWebDriver(),
						shadowElement.findElement(By.cssSelector(NabPO.FirstName)), columns.get("FirstName"));
				DriverHelper.shadowElementTypeText(Hooks.testDriver.getWebDriver(),
						shadowElement.findElement(By.cssSelector(NabPO.LastName)), columns.get("LastName"));
				DriverHelper.shadowElementTypeText(Hooks.testDriver.getWebDriver(),
						shadowElement.findElement(By.cssSelector(NabPO.PreferedName)),
						columns.get("PreferredFirstName"));
				DriverHelper.shadowElementTypeText(Hooks.testDriver.getWebDriver(),
						shadowElement.findElement(By.cssSelector(NabPO.Email)), columns.get("Email"));
				DriverHelper.shadowElementTypeText(Hooks.testDriver.getWebDriver(),
						shadowElement.findElement(By.cssSelector(NabPO.MobileNumber)), columns.get("MobileNumber"));
			}
			DriverHelper.scrollbyview(Hooks.testDriver.getWebDriver(),
					shadowElement.findElement(By.cssSelector(NabPO.NextButton)));
			DriverHelper.shadowElementClick(Hooks.testDriver.getWebDriver(),
					shadowElement.findElement(By.cssSelector(NabPO.NextButton)));
			logger.info("Getting confirmation...........");
			DriverHelper.scrollbyview(Hooks.testDriver.getWebDriver(),
					shadowElement.findElement(By.cssSelector(NabPO.AppointmentButton)));
			DriverHelper.shadowElementClick(Hooks.testDriver.getWebDriver(),
					shadowElement.findElement(By.cssSelector(NabPO.AppointmentButton)));
			logger.info("Getting Text...........");
			RemoteWebElement confirmation = MiscHelper.handlingShadowElementInDOM(NabPO.ParentShadowelement,
					Hooks.testDriver.getWebDriver());
			String confirmationMessage = confirmation.findElement(By.cssSelector(NabPO.ConfirmationMessage)).getText();
			logger.info("Confirmation Message on Screen: " + confirmationMessage);
			if (confirmationMessage.contains("appointment will be")) {
				isAppointmentbooked = true;
			}

		} catch (Exception e) {
			logger.error("Landed in Catch block", e);
		}

		logger.info("Exit fillTheContactDetails function");
		return isAppointmentbooked;
	}

}

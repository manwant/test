package stepdefinitions;


import java.util.List;
import java.util.Map;

import org.junit.Assert;

import Implementation.NabImp;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

public class Nabdef {
	
	
	@Given("Open the NAB application.")
	public void open_the_nab_application() {
		Assert.assertTrue(NabImp.openingNabURL());
	}

	@And("Select Personal tab and then choose Home Loan options.")
	public void select_personal_tab_and_then_choose_home_loan_options() {
		Assert.assertTrue(NabImp.selectingPersonalTabandClickHomeLoans());
	}
	
	@Then("Select Book An Appointment Option and click Buying an Property Option.")
	public void select_book_an_appointment_option_and_click_buying_an_property_option() {
		Assert.assertTrue(NabImp.clickingOnBookAppointmentOptionandSelectBuyProperty());
	}
	
	@Then("Select an Applicant and click Next Button.")
	public void select_an_applicant_and_click_next_button() {
		Assert.assertTrue(NabImp.selectanApplicantAndClickNextButton());
	}
	
	@And("Choose Income with Deposit and click Next Button.")
	public void choose_income_with_deposit_and_click_next_button() {
		Assert.assertTrue(NabImp.selectIncomeAndClickNextButton());
		Assert.assertTrue(NabImp.selectDepositAndClickNextButton());
		
	}
	
	@Then("Enter Suburb {string} and Click Next Button.")
	public static void enter_suburb_and_click_next_button(String suburbName) {
		Assert.assertTrue(NabImp.enterSuburbAndClickNextButton(suburbName));
	}
	
	@And("Select Over the Phone Option with Date and Time.")
	public static void Select_over_the_phone_option_with_date_and_time() {
		Assert.assertTrue(NabImp.selectOverthePhoneOptionwithDateTime());
	}
	
	@Then("Fill the following Contact Details and check for Confirmation Message.")
	public void fill_contact_details(DataTable table) {
		List<Map<String, String>> rows = table.asMaps(String.class, String.class);
	    Assert.assertTrue(NabImp.fillTheContactDetails(rows));
	    
	}
}

package stepdefinitions;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(features = { "./src/test/resources/featurefiles" }, plugin = { "pretty",
		"html:cucumber-html-report", "json:cucumber.json",
		"com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:" },glue={"stepdefinitions"} ,dryRun = false, monochrome = true, tags = "@Smoke" ,publish = true)

public class CucumberRunner {
	

}

package stepdefinitions;


import org.junit.Assert;

import Implementation.APImpl;
import io.cucumber.java.en.Given;

public class APIdef {
	
	
	@Given("Hit the Current Weather API with URL {string} and lat {string} long {string}")
	public void hit_the_current_weather_api_with_url_and_lat_long(String endpointName, String lat, String lon) {
	    Assert.assertTrue(APImpl.GETCurrentWeatherDataAPI(endpointName,lat,lon));
	}
	
	@Given("Hit the Hourly Forecast API with URL {string} and postalCode {string}")
	public void hit_the_hourly_forecast_api_with_url_and_postalcode(String endpointName, String postalCode) {
	    Assert.assertTrue(APImpl.GETHourlyForecastDataAPI(endpointName,postalCode));
	}

	
}

package stepdefinitions;

import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.support.PageFactory;
import helpers.TestDriver;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import pageobjects.NabPO;

public class Hooks {

	public static TestDriver testDriver;
	private static Logger logger = LogManager.getLogger(Hooks.class);

	@Before
	public void browserOpen(Scenario scenario) {
		String ScenarioName = scenario.getName();
		logger.info("Name of Scenario in Before Hooks: " + ScenarioName);
		if (ScenarioName.contains("API")) {
			logger.info("No need to open the Browser");
		} else {
			logger.info("Opening Browser");
			testDriver = new TestDriver();
			testDriver.initialize();
			PageFactory.initElements(Hooks.testDriver.getWebDriver(), NabPO.class);
			logger.info("Browser Opened");
		}

	}

	@After
	public static void embedScreenshot(Scenario scenario) throws IOException, InterruptedException {
		logger.info("Scenarion name: " + scenario + "\n" + "Scenerio: " + scenario.getStatus());
		String ScenarioName = scenario.getName();
		logger.info("Name of Scenario: " + ScenarioName);
		String scenarionameaftertrim = ScenarioName.replace(" ", "");
		logger.info("Scenario after trim:  " + scenarionameaftertrim);
		if (scenario.isFailed()) {
			logger.info("Test Scenarios Failed");
			try {
				String screenshotFilePath = System.getProperty("user.dir") + "\\FailedScreenshots\\"
						+ scenarionameaftertrim + ".png";
				logger.info(screenshotFilePath);
				byte[] screenshot = ((TakesScreenshot) testDriver.getWebDriver()).getScreenshotAs(OutputType.BYTES);
				@SuppressWarnings("resource")
				FileOutputStream fileOuputStream = new FileOutputStream(screenshotFilePath);
				fileOuputStream.write(screenshot);
				scenario.attach(screenshot, "image/png", testDriver.getWebDriver().getCurrentUrl());
			} catch (WebDriverException somePlatformsDontSupportScreenshots) {
				logger.error("Inside Platform Support Screenshot Exception: "
						+ somePlatformsDontSupportScreenshots.getMessage());
			}
		} else {
			logger.info("Test Scenario Got Passed");

		}
		if (ScenarioName.contains("API")) {
			logger.info("No need to close the Browser");
		} else {
			logger.info("Need to close the Browser");
			testDriver.getWebDriver().close();
		}
		logger.info("**********Test Scenarios END********************");

	}

}

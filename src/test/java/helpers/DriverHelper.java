package helpers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DriverHelper {
	private static Logger logger = LogManager.getLogger(DriverHelper.class);

	public static void click(WebDriver driver, WebElement element) {
		MiscHelper.isElementDisplayed(driver, element);
		driver.switchTo().activeElement();
		element.click();
		logger.info("Element has been Clicked");

	}

	public static void waitForElement(WebDriver driver, WebElement element, int timeOutInSeconds) {
		try {
			logger.info("Waiting.... for Element: "+element);
			WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);
			element = wait.until(ExpectedConditions.elementToBeClickable(element));
		} catch (Exception e) {
			logger.error("Exception waiting for element.", e);
		}
	}

	public static void scrollbyview(WebDriver driver, WebElement element) {
		logger.info("Scrolling...by View");
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);
		logger.info("Scroll Done");

	}
	
	public static void shadowElementClick(WebDriver driver, WebElement element) {
		driver.switchTo().activeElement();
		sleep(1);
		element.click();
		logger.info("Element has been Clicked");

	}
	
	public static void sleep(int timeout) {
		try {
			Thread.sleep(timeout *1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}
	
	public static void shadowElementTypeText(WebDriver driver, WebElement element,String text) {
		driver.switchTo().activeElement();
		sleep(1);
		element.sendKeys(text);
		logger.info("Text has been Entered.");

	}
}

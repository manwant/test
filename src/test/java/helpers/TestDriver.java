package helpers;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TestDriver {
	
	private static Logger logger = LogManager.getLogger(TestDriver.class);
	private WebDriver webDriver = null;
	
	  /** Enum holding the browser type.
	   *
	   */
	  enum Browser {
	    CHROME
	  }
	  
	  
	public WebDriver getWebDriver() {
	    return webDriver;
	  }

	  public void setWebDriver(WebDriver webDriver) {
	    this.webDriver = webDriver;
	  }

	  public void initialize() {
	    try {
	      setupWebDriver();
	    } catch (Exception e) {
	      logger.error("Driver initialization failed.", e);
	    }
	  }

	  /** Use this method to set up the web driver for testing.
	   *
	   */
	  private void setupWebDriver() {
	      logger.info("The webDriver object is null, so getting Driver Object from Driver Class.");
	      switch (setBrowser(ConfigReader.getValue("Browser").trim())) {
	        case CHROME:
	          logger.info("Browser chrome is requested by user...");
			  System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\drivers\\chromedriver.exe");
	          System.setProperty("webdriver.chrome.whitelistedIps", "");
	          ChromeOptions options = new ChromeOptions();
	          DesiredCapabilities cap= new DesiredCapabilities();
	          cap.setCapability("resolution", "1920X1080");
	          cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
	          if (Boolean.parseBoolean(ConfigReader.getValue("BrowserHeadless"))) {
	            logger.info("Browser chrome requested as headless mode.");
	            options.addArguments("--headless");
	            options.addArguments("--no-sandbox");
	            options.addArguments("--disable-gpu");
	          }
	          options.setCapability(CapabilityType.PLATFORM_NAME, Platform.WINDOWS);
	          options.addArguments("--start-maximized");
	          options.addArguments("disable-infobars");
	          options.setAcceptInsecureCerts(true);
	          options.addArguments("--no-sandbox");
	          options.addArguments("--whitelisted-ips");
	          options.addArguments("--disable-web-security");
	          options.addArguments("--ignore-certificate-errors");
	          options.addArguments("--disable-site-isolation-for-policy");
	          options.merge(cap);
	          webDriver= new ChromeDriver(options);
	          setWebDriver(webDriver);
	          break;
	        default:
	          logger.fatal("Unsupported browser type in configuration." + " Please check for a supported value.");
	      }
	  }
	  
	  /**
	   * Based on browser type defined in the configuration, the BROWSER value will be returned.
	   * @param browser browser string value
	   * @return Browser enum value.
	   */
	  private Browser setBrowser(String browser) {
		  Browser browserToSelect = null;
	    if (browser.equalsIgnoreCase("chrome")) {
	    	browserToSelect= Browser.CHROME;
	    } 
	    return browserToSelect;
	  }

}

package helpers;

import org.apache.logging.log4j.LogManager;
import java.util.Map;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.RemoteWebElement;
import stepdefinitions.Hooks;

public class MiscHelper {
	
	private static Logger logger = LogManager.getLogger(MiscHelper.class);
	
	
	
	public static void domLoading(WebDriver driver) {
		Boolean readyStateComplete = false;
		Boolean readyStateInteractive = false;
		while (!readyStateComplete && !readyStateInteractive)
	    {
			logger.info("Waiting for DOM to be loaded");
	        JavascriptExecutor executor = (JavascriptExecutor) driver;
	        String st = executor.executeScript("return document.readyState").toString();
	        logger.info("DOM Status coming is: "+ st);
	        readyStateComplete = executor.executeScript("return document.readyState").toString().equals("complete");
	        readyStateInteractive = executor.executeScript("return document.readyState").toString().equals("interactive");
	        logger.info("DOM ready state flag for complete: "+ readyStateComplete);
	        logger.info("DOM ready state flag for interactive: "+ readyStateInteractive);
	    }
		logger.info("DOM loaded successfully");
	}

	
	@SuppressWarnings("unchecked")
	public static RemoteWebElement handlingShadowElementInDOM(WebElement parentShadowElement, WebDriver driver) { 
		MiscHelper.domLoading(Hooks.testDriver.getWebDriver());
		DriverHelper.sleep(3);
		Object shadowRoot = ((JavascriptExecutor) driver).executeScript("return arguments[0].shadowRoot", parentShadowElement);
		String id = (String) ((Map<String, Object>) shadowRoot).get("shadow-6066-11e4-a52e-4f735466cecf"); 
		RemoteWebElement shadowRootElement = new RemoteWebElement(); 
		shadowRootElement.setParent((RemoteWebDriver) driver); 
		shadowRootElement.setId(id);
		DriverHelper.sleep(2);
		return shadowRootElement;
	}
	
	public static boolean checkElementVisibilty(WebDriver driver ,String script) {
		int counter = 0;
		boolean isElementVisible=false;
		while(counter!=1000) {
	        JavascriptExecutor executor = (JavascriptExecutor) driver;
	        String length = executor.executeScript("return document."+script).toString();
	        logger.info("Length of Element on Screen: "+ length);
	        if(Integer.parseInt(length)>=1) {
	        	logger.info("Element is Visible");
	        	isElementVisible=true;
	        	break;
	        }
	        else {
	        	logger.info("Element is not Visible keep checking");
	        	counter++;
	        	logger.info("Value in Counter: "+ counter);
	        }
		
		}
		return isElementVisible;	
	}
	
	public static boolean isElementDisplayed(WebDriver driver ,WebElement element) {
		logger.info("Element coming: "+ element);
		int counter = 0;
		boolean isElementVisible=false;
		while(counter!=1000) {
			try {
				logger.info("Checking in Try Block");
				isElementVisible = element.isDisplayed();
			}catch(Exception e) {
				logger.info("Landed in catch block while checking");
			}
			if(isElementVisible) {
				 logger.info("Element is Visible");
				 DriverHelper.waitForElement(driver, element, 1000);
				 break;
			}
			else {
				counter++;
				logger.info("Element is not Visible, keep checking: "+counter);
				
			}
		}
		return isElementVisible;	
	}
	
	public static int retrundayasperAPPCalender(String monthName) {
		int number = 0;
		if (monthName.equalsIgnoreCase("Monday")) {
			number = 6;
		} else if (monthName.equalsIgnoreCase("Tuesday")) {
			number = 7;
		} else if (monthName.equalsIgnoreCase("Wednesday")) {
			number = 1;
		} else if (monthName.equalsIgnoreCase("Thursday")) {
			number = 2;
		} else if (monthName.equalsIgnoreCase("Friday")) {
			number = 3;
		} else if (monthName.equalsIgnoreCase("Saturday")) {
			number = 4;
		} else if (monthName.equalsIgnoreCase("Sunday")) {
			number = 5;
		}
		return number;
	}
}

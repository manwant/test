package pageobjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class NabPO {

	public final static String PersonalOption = "//header[@class='nab-header-bar__header']//descendant::li[@class='navigation__item--level-1 active']";
	public final static String HomesLoans = "//ul[@class='dropdown mega-menu active']//descendant::a[@data-menulevel='2']//span[contains(text(),'Home loans')]";
	public final static String HomesLoansSubMenu= "//ul[@class='dropdown mega-menu active']//li[@class='navigation__item--level-2 active']//descendant::li[@class='navigation__item--level-3 parent']";
	public final static String BookAnAppointment= "//div[@class='responsivegrid nab-banner__primary-content']//descendant::li[@class='cmp-list__item'][1]";
	public final static String ParentShadowElement="//*[contains(@x-instance-id,'ky')]";
	public final static String BuyingAProperty="div[class*='ListItemstyle__StyledListItemContainer']";
	public final static String OneApplicant="input[class*='CustomInput-enkbNl']";
	public final static String FullORPartTimeIncome="input[id='income-0']";
	public final static String MoneySavedDeposit="input[id='deposit-0']";
	public final static String SubUrb="input[name='location']";
	public final static String SelectSubUrb="li[id='autosuggest-listbox-location-0']";
	public final static String OverthePhone="div[class*='ListItemstyle__StyledListItemContainer']";
	public final static String NoAvailableTime="form > div:nth-child(4) > div > div.sc-gtsrHT.Rowstyle__StyledRow-mjf486-0.iHXuUE.hMGLfV > div > div:nth-child(1) > div > p";
	public final static String DateToSelect="#date-202201xxx";
	public final static String MorningTimeToSelect="form > div:nth-child(4) > div > div:nth-child(3) > div > div:nth-child(2) > div:nth-child(1) > button";
	public final static String NoonTimeToSelect="form > div:nth-child(4) > div > div:nth-child(4) > div > div:nth-child(2) > div:nth-child(1) > button";
	public final static String EveningTimeToSelect="form > div:nth-child(4) > div > div.Containerstyle__StyledWrapper-sc-10ei1r0-0.kOMZfd > div > div:nth-child(2) > div:nth-child(1) > button";
	public final static String FirstName="input[id='firstName']";
	public final static String LastName="input[id='lastName']";
	public final static String PreferedName="input[id='preferredName']";
	public final static String Email="input[id='email']";
	public final static String MobileNumber="input[id='mobile']";
	public final static String ConfirmationMessage="div.sc-dlnjwi.Colstyle__StyledCol-sc-1evc4kf-0.ldCTSc.duEYaW.LeftContentCol-fOupiG.TimeLineCol-kkbsXL.fKiecM.gtCySW > div:nth-child(1) > div > div > div.sc-dlnjwi.Colstyle__StyledCol-sc-1evc4kf-0.ctOqKu.jvKLKX > h1";
	public final static String AppointmentButton="button[class*='Buttonstyle__StyledButton']";
	public final static String NextButton="button[type='submit']";
	public final static String SubUrbNextButton="button[type='button']";
	public final static String DateTimeNextButton2="div.sc-dlnjwi.Colstyle__StyledCol-sc-1evc4kf-0.ldCTSc.duEYaW.LeftContentCol-fOupiG.SelectTimeSlotCol-kxmyod.fKiecM.dUxGgZ > p > button";
	
	
	/**
	 * these are the Page Factory element locators
	 */
	@FindBy(how = How.XPATH, using = PersonalOption)
	public static WebElement Personaloption;

	@FindBy(how = How.XPATH, using = HomesLoans)
	public static WebElement Homesloans;

	@FindBy(how = How.XPATH, using = HomesLoansSubMenu)
	public static WebElement HomesLoansSubmenu;

	@FindBy(how = How.XPATH, using = BookAnAppointment)
	public static WebElement BookAnappointment;

	@FindBy(how = How.XPATH, using = ParentShadowElement)
	public static WebElement ParentShadowelement;
}
